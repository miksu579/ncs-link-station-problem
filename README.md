# NCS Link station problem

- A program that solves the most suitable (with most power) link station for a device at given point [x,y].
- React, Node.js (express) stack with server on AWS EC2
- The next step of developing this project could be integrating CI/CD pipeline to make the development process simpler.

- **Front end application URI: https://ncs.mikkoahola.fi**
- **API**
    - **URI: https://ncs.mikkoahola.fi/api**
    - Available http requests:
        - POST: /evaluatebestlinkstation
            - You can test back end api for example with Postman
            - Example data for single point: 
            ```javascript
            {"linkstations": [[0, 0, 10],[20, 20, 5],[10, 0, 12]], "points": [0, 0]}
            ```
            
            - Example data for multiple points:
             ```javascript
             {"linkstations": [[0, 0, 10],[20, 20, 5],[10, 0, 12]], "points": [[0, 0],[100, 100],[15, 10],[18, 18]]}
            ```
## Front end

The front end is based on React. The file structure is divided into component folders to keep everything in order. Please follow the same pattern as you develop the application.

### Development 
* Download and install Node.js from : https://nodejs.org/en/
* `git clone https://gitlab.com/miksu579/ncs-link-station-problem.git`
* `git checkout dev`
* `cd /ncs-link-station-problem/frontend`
* `npm install`
* `npm start`
* Front end is running on localhost:3000

### Into production
* `cd /ncs-link-station-problem/frontend`
* `npm run build`
* Deploy the insides of the build folder into aws ec2 with the way of your liking.


## Back end

The back end is built on Node.js express. The apllication architecture is based on three layer architecture. Please follow the same architectual style as you develop the application.

### Development 

* Download and install Node.js from : https://nodejs.org/en/
* `git clone https://gitlab.com/miksu579/ncs-link-station-problem.git`
* `git checkout dev`
* `cd ncs-link-station-problem/backend`
* `npm install`
* `node app.js`
* Backend is running on localhost:5000
* You can test the back end for example with Postman
    * Input data in format where x and y are coordinates (number) and r is linkstation reach (number).
    * Example data format for single point:
    ```javascript
        {"linkstations": [[x,y,r], [x,y,r]], "points": [x,y]}
    ```
    * Example data format for multiple points:
    ```javascript
        {"linkstations": [[x,y,r], [x,y,r]], "points": [[x,y],[x,y]]}
    ```

### Into production
* Push into master
* Git pull at aws ec2
* Pm2 restart app.js at aws ec2
