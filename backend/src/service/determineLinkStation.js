const  determineLinkStation = {
    //calculates the distance between two points in two dimensional space
    //point1 : Object {x:number, y:number}
    //point2 : Object {x:number, y:number}
    calculateDistanceBetweenTwoPoints : (linkStation, point) =>{
        //use application of pythagorean theorem to determine the distance between two points on two dimensional plane
        return Math.sqrt(
            Math.pow(linkStation.x - point.x, 2) + Math.pow(linkStation.y - point.y, 2)
          );
    },
    //calculates power that is needed for determing the best link station for certain coordinates
    //the math was given in specs
    calculatePower : (distanceToLinkStation, linkStationReach) =>{
        if (distanceToLinkStation > linkStationReach) return 0;
        return Math.pow(linkStationReach - distanceToLinkStation, 2);
    },
    //determine the linkstation that is best for the certain point in space
    //linkStations : [[x,y,r],[x,y,r]...]
    //point: [x,y]
    determineMostSuitableLinkStation : (linkStations, point) =>{
        // initiating mostSuitableLinkStation for future use
        let mostSuitableLinkStation = { x: 0, y: 0, power: 0 };
        linkStations.forEach((linkStation) => {
        //calculate each link stations distance to the point
        let distanceToLinkStation = determineLinkStation.calculateDistanceBetweenTwoPoints(
            { x: linkStation[0], y: linkStation[1]},
            { x: point[0], y: point[1] }
            );
        let power = determineLinkStation.calculatePower(distanceToLinkStation, linkStation[2]);
        //if the current linkstations power is greater than the ones earlier, set this link station the most suitable
        if (power >= mostSuitableLinkStation.power)
            mostSuitableLinkStation = { x: linkStation[0], y: linkStation[1], power };
        });
        //check what to print out for the user
        if (mostSuitableLinkStation.power === 0)
        return `No link station within reach for point ${point[0]},${point[1]}`;
        return `Best link station for point ${point[0]},${point[1]} is ${mostSuitableLinkStation.x},${mostSuitableLinkStation.y} with power ${mostSuitableLinkStation.power}`;
    }
}
module.exports = determineLinkStation;