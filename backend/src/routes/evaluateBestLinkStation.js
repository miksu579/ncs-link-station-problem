const express = require('express');
const determineLinkStation = require('../service/determineLinkStation');
const evaluateBestLinkStation = express.Router();
const { body, validationResult } = require('express-validator');

evaluateBestLinkStation.post('/'
//custom validation for points with express-validator
,body('points').custom(points =>{
  //if there is only one coordinate
  if(!Array.isArray(points[0])){
    points.forEach((point, pointIndex) => {
      if(isNaN(point)) throw new Error(`coordinate at index ${pointIndex} is not a number!`)
    });
    return true;
  }else{
    //if there are multiple coordinates
    if(!Array.isArray(points))throw new Error('points is not an array!')
    points.forEach((point, pointIndex) => {
      if(!Array.isArray(point)) throw new Error(`Point at index ${pointIndex} is not an array!`)
      point.forEach((item, index) =>{
        if(isNaN(item)) throw new Error(`coordinate at index ${index} of Point at index ${pointIndex} is not a number!`) 
      })
    });
    return true;
  }
})
//custom validation for link stations with express-validator
,body('linkstations').custom(linkStations =>{
  if(!Array.isArray(linkStations))throw new Error('linkStations is not an array!')
  linkStations.forEach((linkStation, linkStationIndex) => {
    if(!Array.isArray(linkStation)) throw new Error(`linkStation at index ${linkStationIndex} is not an array!`)
    linkStation.forEach((item, index) =>{
      if(isNaN(item)) throw new Error(`coordinate at index ${index} of linkStation at index ${linkStationIndex} is not a number!`)
    })
  });
  return true;
}),(req, res) => {
  //if there were errors, send them back to user
  //else start determing most suitable link stations.
  const errors = validationResult(req);
  if(!errors.isEmpty()){
    return res.json({ errors: errors.array() });
  }else{
    let arr = [];
    //if there is only one point, determine only one best link station, else determine best link station for every point
    if(req.body.points[0].length === undefined){
      res.send(determineLinkStation.determineMostSuitableLinkStation(req.body.linkstations, req.body.points))
    }else{
      req.body.points.forEach(point => {
            arr.push(determineLinkStation.determineMostSuitableLinkStation(req.body.linkstations, point))
      });
        res.send(arr);
    }
  }
})

module.exports = evaluateBestLinkStation;
