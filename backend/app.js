const express = require('express');
const app = express();
require('dotenv').config();
const port = process.env.SERVER_PORT;
const welcome = require('./src/routes/welcome');
const evaluateBestLinkStation = require('./src/routes/evaluateBestLinkStation');

//CORS handling
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept, Auhtorization"
    );
    if (req.method === "OPTIONS") {
      res.header("Access-Control-Allow-Methods", "POST, GET");
      return res.status(200).json({});
    }
    next();
  });
app.use(express.json());

app.use('/', welcome);

app.use('/evaluatebestlinkstation', evaluateBestLinkStation);


app.listen(port, () => {
  console.log(`NCS link station app listening at http://localhost:${port}`)
})