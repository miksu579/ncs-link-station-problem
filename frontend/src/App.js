import Main from "./components/main/Main";
import React, { Component } from "react";
import ncsAxios from './utils/ncsAxios';

class App extends Component {
  state = {
    errorMessages: [],
    listOfBestLinkStations: [],
    linkStationAreaValue: "[[0, 0, 10],[20, 20, 5],[10, 0, 12]]",
    pointAreaValue: "[[0, 0],[100, 100],[15, 10],[18, 18]]",
  };

  //simple front end data validation
  validateData = (linkStationList, pointList) => {
    let parsedLinkStations;
    let parsedPoints;
    //wanted form of input is JSON like, so it should be possible to parse it.
    try {
      parsedLinkStations = JSON.parse(linkStationList);
    } catch (error) {
      alert(
        "please fill in link station data in form [[x,y,r],[x,y,r]...] where x is x-coordinate, y is y-coordinate and r is reach!"
      );
      console.log(error);
      return null;
    }
    try {
      parsedPoints = JSON.parse(pointList);
    } catch (error) {
      alert(
        "please fill in point data in form [[x,y],[x,y]...] where x is x-coordinate, y is y-coordinate!"
      );
      console.log(error);
      return null;
    }
    let parsedData = [
      { linkStations: parsedLinkStations },
      { points: parsedPoints },
    ];
    return parsedData;
  };

  //sending data to backend should always work asynchronously to ensure data arrival before continuing.
  sendData = async (data) => {
    //post request with object that contains "linkstations" Array and "points" Array.
    const response = await ncsAxios.post(`/evaluatebestlinkstation`, {
      linkstations: data[0].linkStations,
      points: data[1].points,
    });
    //After receiving the response check if errors occurred in the backend.
    //if errors occurred, set errors to state, else set list of best link stations to state.
    this.setState({errorMessages : []})
    if (response.data.errors) {
      let errorMessages = [];
      response.data.errors.forEach((error) => {
        errorMessages.push(error.msg);
      });
      this.setState({ errorMessages });
    } else {
      if(!Array.isArray(response.data)){
        this.setState({ listOfBestLinkStations: [response.data] });
      } else{
        this.setState({ listOfBestLinkStations: response.data });
      }
    }
  };

  //change state when link station text area value changes
  linkAreaChange = (e) => {
    this.setState({ linkStationAreaValue: e.target.value });
  };

  //change state when point text area value changes
  pointAreaChange = (e) => {
    this.setState({ pointAreaValue: e.target.value });
  };

  render() {
    return (
      <div className="App">
        {/* if there are no errors in the state, render best link stations, else render errors */}
        {this.state.errorMessages.length === 0 ? (
          <Main
            pointAreaChange={this.pointAreaChange}
            validateData={this.validateData}
            linkAreaChange={this.linkAreaChange}
            sendData={this.sendData}
            pointAreaValue={this.state.pointAreaValue}
            linkStationAreaValue={this.state.linkStationAreaValue}
            messageList={this.state.listOfBestLinkStations}
          ></Main>
        ) : (
          <Main
            pointAreaChange={this.pointAreaChange}
            validateData={this.validateData}
            linkAreaChange={this.linkAreaChange}
            sendData={this.sendData}
            pointAreaValue={this.state.pointAreaValue}
            linkStationAreaValue={this.state.linkStationAreaValue}
            messageList={this.state.errorMessages}
          ></Main>
        )}
      </div>
    );
  }
}

export default App;
