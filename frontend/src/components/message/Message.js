import "../../styles/message/styles.css";
import React from 'react';

//A very simple message output for a single message
function Message (props) {
    
        return(
            <div className="message-container">
                {props.text}
            </div>
        );
    }

export default Message;