import "../../styles/main/styles.css";
import React, { Component } from 'react';
import MessageList from '../messageList/MessageList';

class Main extends Component{
    validateAndSend = () =>{
        let validatedData = this.props.validateData(this.props.linkStationAreaValue, this.props.pointAreaValue);
        //if data is valid in the front end, send it to backend
        if(validatedData)this.props.sendData(validatedData);
    }
    
    render(){
        return(
            <div className="main-container">
                <div className="tutorial">Welcome! Here's how this works: fill in link stations and points with given format, then click the send request button.</div>
                {/* container for the link station area and everything linked to it */}
                <div className="link-station-container area">
                <div className="link-station-help">Please fill in link station data in format: [[x,y,r],[x,y,r]...] where x is x-coordinate, y is y-coordinate and r is reach</div>
                <label htmlFor="link-stations">Link stations:</label>
                <textarea className="link-station-area" id="link-stations" value={this.props.linkStationAreaValue} onChange={this.props.linkAreaChange} ></textarea>
                </div>
                {/* container for the point area and everything linked to it */}
                <div className="point-container area">
                <div className="point-help">Please fill in point data in format: [[x,y],[x,y]...] where x is x-coordinate, y is y-coordinate. You can also fill in a single point in format: [x,y]</div>
                <label htmlFor="points">Points:</label>
                <textarea className="point-area" id="points" value={this.props.pointAreaValue} onChange={this.props.pointAreaChange} ></textarea>
                </div>
               
                <button onClick={this.validateAndSend}>Send request</button>
          
                <MessageList messageList={this.props.messageList}></MessageList>
            </div>
        );
    }
}
export default Main;