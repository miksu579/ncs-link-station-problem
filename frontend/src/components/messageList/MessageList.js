import React from 'react';
import Message from '../message/Message';

//List out all messages to output
function MessageList (props){
    
        return(
            <div className="message-list-container">
                {props.messageList.map( (linkStation, index) =>{
                    return <Message text={linkStation} key={index}/>
                })}
            </div>
        );
    }

export default MessageList;